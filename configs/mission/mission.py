#!/usr/bin/env python3

import mission_execution_control as mxc
import rospy
import math
import sys
'''
This is a simple mission, the drone takes off, follows a path and lands
1.startTask. Starts the task and continues.
2.executeTask. Starts the task and waits until it stops.
'''
def mission():
  print("Starting mission...")

  print("Taking off...")
  mxc.executeTask('TAKE_OFF')

  print("ROTATE...")
  mxc.executeTask('ROTATE', relative_angle = 60)

  print("FOLLOW_PATH...")
  radius = 10
  step = 0.05
  resolution=.05
  angle=0.0
  start=0.0
  dist = start+0.0
  coords=[]
  altitude = 1
  while dist*math.hypot(math.cos(angle),math.sin(angle))<radius:
      cord=[]
      cord.append(dist*math.cos(angle))
      cord.append(dist*math.sin(angle))
      cord.append(altitude)
      coords.append(cord)
      altitude+=0.1
      dist+=step
      angle+=resolution
  mxc.executeTask('FOLLOW_PATH', path = coords)
  #mxc.executeTask('FOLLOW_PATH', path = [ [0, 0, 1], [0, 20, 1] , [20, 20, 1] , [20, 0, 5] , [0, 0, 1] ])

  print("Landing...")
  mxc.executeTask('LAND')

  print('Finish mission...')
